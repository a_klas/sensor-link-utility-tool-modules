# SLUT_SENSORS


Repozytorium zawierające listę modułów czujników wykorzystywanych na zajęciach SWAW.

Moduły dostępne są w trzech rozmiarach określonych przez firmę MikroElektronika:

- S  [25.4x28.6 mm];
- M  [25.4x42.9 mm];
- S  [25.4x57.15 mm].

Wytyczne dostępne są na poniższej stronie:
https://download.mikroe.com/documents/standards/mikrobus/mikrobus-standard-specification-v200.pdf

Płytki PCB nowych modułów NALEŻY wykonać na podstawie wzorów zawartych w folderze SENSOR_TEMPLATES.
Wzory dostępne są w trzech programach:

- Altium Designer
- EAGLE [SOON]
- KiCad [SOON]

W przypadku ambitniejszych projektów wymagających większych płytek lub wykorzystujących kilka slotów na płycie MOTHERBOARD, należy skonsultować się z prowadzącym zajęcia.

**UWAGA:**

Stworzyłeś/-aś  swój własny moduł i chcesz się nim podzielić? Jeśli tak to przygotuj repozytorium trzymając się poniższych wytycznych:

- Nazwa repo: MIKROBUS_(*NAZWA SENSORA*);
- Repozytorium powinno zawierać dwa foldery (HARDWARE i FIRMWARE); 
- Opisz krótko swój moduł w pliku README.

Jeśli uważasz, że spełniasz wymagania podeślij link do prowadzącego zajęcia lub @a_klas. Sprawdzimy Twój projekt i dodamy jako submoduł lub damy feedback co należy poprawić ;)
  